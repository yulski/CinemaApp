import { Coordinates } from './coordinates';
import { DataContainer } from './data-container';

export class Cinema implements DataContainer {

    constructor(public id: number, public name: string, public city: string, public county: string,
                public coords: Coordinates) {
    }
}
