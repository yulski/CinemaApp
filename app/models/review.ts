import { DataContainer } from './data-container';
import { Movie } from './movie';
import { User } from './user';

export class Review implements DataContainer {

    constructor(public id: number, public title: string, public body: string, public rating: number,
                public movie: Movie, public author: User) {
    }

    get halfStar(): boolean {
        return this.rating % 1 >= 0.5;
    }

    get stars(): number[] {
        return new Array(Math.floor(this.rating));
    }
}
