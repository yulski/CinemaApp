import { DataContainer } from './data-container';

export class Movie implements DataContainer {
    // TODO: add reviews: Review[]

    constructor(public id: number, public title: string, public rating: number, public length: number, public director: string,
                public cast: string[], public images: string[], public trailer: string) {
    }

    // returns true of this movie's rating has a half-star at the end
    get halfStar(): boolean {
        return this.rating % 1 >= 0.5;
    }

    // returns an array representation of rating
    get stars(): number[] {
        return new Array(Math.floor(this.rating));
    }

}
