// interface for all data classes that can
// have instances saved in the database
export interface DataContainer {
    // if its in the database it must
    // have an id
    id: number;
}
