import { DataContainer } from './data-container';

export class User implements DataContainer {

    public profilePicture: string;
    public reviews: number[];

    constructor(public id: number, public fullName: string, public username: string, public email: string,
                public password: string, reviews?: number[], profilePicture?: string) {
        // if no profile picture was passed, resort to default
        this.profilePicture = profilePicture? profilePicture : './images/profiles/profile_picture_1.png';
        // if no reviews passed, set empty array
        this.reviews = reviews? reviews : [];
    }


}