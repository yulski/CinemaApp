// the styles with which a message can be displayed
export enum MessageStyles {
    NONE, PRIMARY, SUCCESS, DANGER
}
