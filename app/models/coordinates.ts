// represents a set of geographical coordinates
export interface Coordinates {
    lat: number;
    lng: number;
}
