import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';

import { AgmCoreModule } from 'angular2-google-maps/core';
import { CoolStorageModule } from 'angular2-cool-storage';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';

import { AppComponent } from './components/app/app.component';
import { routing } from './app.routing';
import { AutocompleteComponent } from './components/shared/autocomplete/autocomplete.component';
import { CinemaComponent } from './components/cinema/cinema.component';
import { CinemaListComponent } from './components/cinema/list/cinema-list.component';
import { CinemaService } from './services/cinema.service';
import { googleMapsApiKey } from './credentials';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DataService } from './data.service';
import { FooterComponent } from './components/shared/footer/footer.component';
import { LoginComponent } from './components/shared/login/login.component';
import { MessageService } from './services/message.service';
import { MovieComponent } from './components/movie/movie.component';
import { MovieListComponent } from './components/movie/list/movie-list.component';
import { MovieService } from './services/movie.service';
import { NavigationComponent} from './components/shared/navigation/navigation.component';
import { ReviewComponent } from './components/shared/review/review.component';
import { ReviewService } from './services/review.service';
import { SignupComponent } from './components/shared/signup/signup.component';
import { UserProfileComponent } from './components/profile/user-profile.component';
import { UserService } from './services/user.service';

@NgModule({
    imports: [
        AgmCoreModule.forRoot({apiKey: googleMapsApiKey}),
        BrowserModule,
        CoolStorageModule,
        FormsModule,
        HttpModule,
        InMemoryWebApiModule.forRoot(DataService),
        routing,
    ],
    declarations: [
        AutocompleteComponent,
        AppComponent,
        CinemaComponent,
        CinemaListComponent,
        DashboardComponent,
        FooterComponent,
        LoginComponent,
        MovieComponent,
        MovieListComponent,
        NavigationComponent,
        ReviewComponent,
        SignupComponent,
        UserProfileComponent,
    ],
    bootstrap: [AppComponent],
    providers: [CinemaService, MessageService, MovieService, ReviewService, UserService]
})

export class AppModule {
}
