import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Cinema } from '../../models/cinema';
import { CinemaService } from '../../services/cinema.service';
import { Movie } from '../../models/movie';
import { MovieService } from '../../services/movie.service';
import { Review } from '../../models/review';
import { ReviewService } from '../../services/review.service';

@Component({
    selector: 'dashboard',
    templateUrl: './app/components/dashboard/dashboard.component.html',
    styleUrls: [ './app/components/dashboard/dashboard.component.css' ]
})

export class DashboardComponent implements OnInit {

    // this holds a list of all cinemas
    cinemas: Cinema[] = [];

    // this holds a list of currently showing movies
    movies: Movie[] = [];

    // this holds a list of recent reviews
    reviews: Review[] = [];

    constructor(private cinemaService: CinemaService, private movieService: MovieService,
                private reviewService: ReviewService, private router: Router) {
    }

    ngOnInit(): void {
        // get all cinemas
        this.cinemaService.getAll()
            .then((res: Cinema[]) => {
                this.cinemas = res;
            });
        // get 6 movies to display as currently showing
        this.movieService.getMany(6)
            .then((res: Movie[]) => {
                this.movies = res;
            });
        // get the 5 most recent reviews
        this.reviewService.getMany(5, true)
            .then((res: Review[]) => {
                for (let r of res) {
                    this.reviews.push(
                        new Review(r.id, r.title, r.body, r.rating, r.movie, r.author)
                    );
                }
            });
    }

    // navigates to a movie's page
    navigateToMovie(id: number): void {
        this.router.navigate(['/movie', id]);
    }
}