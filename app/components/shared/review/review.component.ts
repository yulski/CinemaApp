import { Component, Input, OnInit } from '@angular/core';

import { MovieService } from '../../../services/movie.service';
import { Review } from '../../../models/review';
import { ReviewService } from '../../../services/review.service';

@Component({
    selector: 'review',
    templateUrl: './app/components/shared/review/review.component.html',
    styleUrls: [ './app/components/shared/review/review.component.css' ]
})

export class ReviewComponent implements OnInit {

    // determines whether the user is allowed to edit a particular review
    @Input() allowEdit = false;

    // determines whether the user is currently editing the review
    private editingReview: boolean = false;

    // the review displayed inside this component
    @Input() review: Review;

    // a model for the review editing form
    reviewEditModel: Review;

    constructor(private reviewService: ReviewService, private movieService: MovieService) {
    }

    ngOnInit(): void {
        this.reviewEditModel = this.review;
    }

    toggleEditingReview(): void {
        this.editingReview = !this.editingReview;
    }


    hideEditingReview(): void {
        this.editingReview = false;
    }

    // update the review and hide form
    reviewEdited(): void {
        this.editingReview = false;
        this.reviewService.update(this.reviewEditModel)
            .then((res: boolean) => {
                // if (res) {
                //     this.movieService.recalculateRating();
                // }
            });
    }

}
