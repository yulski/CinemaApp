import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { ItemGetter } from '../../../services/item-getter';

@Component({
    selector: 'autocomplete',
    templateUrl: './app/components/shared/autocomplete/autocomplete.component.html',
    styleUrls: ['./app/components/shared/autocomplete/autocomplete.component.css']
})

// An autocomplete consisting of a search field and
// a list of matching items
export class AutocompleteComponent implements OnInit {

    // true when user focused on text field
    focused: boolean = false;

    // getter for getting list items
    @Input() getter: ItemGetter<string>;

    // list of all possible matching items
    @Input() items: string[] = [];

    // text of label next to input
    @Input() labelText: string = 'Search: ';

    // list of items matching current
    matches: string[] = [];

    // the text in the search field
    searchText: string = '';

    // emitter for user selection
    @Output() selectionEmitter: EventEmitter<string> = new EventEmitter<string>();

    // determines whether list is displayed or hidden
    showingList: boolean = false;

    hideList(): void {
        this.showingList = false;
    }

    // get items from getter, set this.items and
    // let the getter handle any errors
    ngOnInit(): void {
        if (this.getter) {
            this.getter.getItems().then((response: string[]) => {
                this.items = response;
            }).catch(this.getter.handleError);
        }
    }

    // handles user input into the text field
    onInput() {
        this.findMatches();
        // if the list can be displayed, display it
        this.showingList = this.canDisplay();
    }

    // handles user clicking on one of the options
    onSelect(item: string) {
        this.searchText = item;
        this.hideList();
        this.emitSelection();
    }

    // sets the focused value
    setFocused(isFocused: boolean): void {
        this.focused = isFocused;
        // hide list if component is not focused
        if (!this.focused) {
            this.hideList();
        } else {
            // if can display, display on focus
            if (this.canDisplay()) {
                this.showList();
            }
        }
    }

    showList(): void {
        this.showingList = true;
    }

    toggleList(): void {
        this.showingList = !this.showingList;
    }

    // returns true if the list can be displayed, false otherwise
    private canDisplay(): boolean {
        // make sure that there is at least 1 character of input and
        // at least 1 matching item and that if there is only 1 match,
        // it's not the same as the input text
        return this.searchText.length > 1 && (this.matches.length > 1 ||
            (this.matches[0] && this.matches[0] !== this.searchText));
    }

    // emits the user's selection
    private emitSelection(): void {
        if (this.searchText.length !== 0) {
            this.selectionEmitter.emit(this.searchText);
        }
    }

    // finds items matching current text and puts them
    // into matches array
    private findMatches() {
        let matchesBuff: string[] = [];
        let iItem: string;
        let iText: string;
        for (let item of this.items) {
            // make the search case-insensitive
            iItem = item.toLowerCase();
            iText = this.searchText.toLowerCase();
            if (iItem.indexOf(iText) === 0) {
                matchesBuff.push(item);
                // limit matches list to a max length of 5
                if (matchesBuff.length > 5) {
                    break;
                }
            }
        }
        this.matches = matchesBuff;
    }
}
