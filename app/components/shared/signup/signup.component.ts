import { Component, EventEmitter, Input, Output } from '@angular/core';

import { MessageStyles } from '../../../models/message-styles';
import { MessageService } from '../../../services/message.service';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';

@Component({
    selector: 'signup',
    templateUrl: './app/components/shared/signup/signup.component.html',
    styles: [ `
        .signup-container .modal-body {
            max-height: none;
        }
    ` ]
})

// the signup form modal
export class SignupComponent {

    // emits messages to parent component
    @Output() emitter: EventEmitter<string> = new EventEmitter<string>();

    // holds the error message if an error occurs
    errorMessage: string = '';

    // the model for the form
    private model = {fullName: '', username: '', email: '', password1: '', password2: ''};

    // determines whether this component should be shown
    // or not
    @Input() show: boolean = false;

    // determines if the error message should be
    // shown or not
    showErrorMessage: boolean = false;

    constructor(private userService: UserService, private messageService: MessageService) {
    }

    // emits a message this this component should be hidden
    cancel(): void {
        this.emitter.emit('cancel');
    }

    hideErrorMessage(): void {
        this.showErrorMessage = false;
    }

    // handles form submission
    onSubmit(): void {
        if (this.model.password1 != this.model.password2) {
            // if passwords do not match, display an error message
            this.errorMessage = 'Your passwords do not match';
            this.showErrorMessage = true;
        } else {
            // create a User object and fill it with data from the form
            let usr = new User(1, this.model.fullName, this.model.username, this.model.email, this.model.password1);
            // try to sign this user up
            this.userService.signUserUp(usr)
                .then((res: boolean) => {
                    if (res) {
                        // if user was signed up successfully, show a success message,
                        // hide this component and reset the form model
                        this.cancel();
                        this.model = {fullName: '', username: '', email: '', password1: '', password2: ''};
                        this.messageService.showMessage('You have signed up successfully. You can log in now.',
                            MessageStyles.SUCCESS);
                    } else {
                        // if signup failed, show an error message
                        this.errorMessage = 'Signup failed. Are you sure you don\'t already have an account?';
                        this.showErrorMessage = true;
                    }
                });
        }
    }
}
