import { Component } from '@angular/core';

@Component({
    selector: 'footer-component',
    templateUrl: './app/components/shared/footer/footer.component.html',
    styleUrls: [ './app/components/shared/footer/footer.component.css' ]
})

export class FooterComponent { }
