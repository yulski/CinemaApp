import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { CountyGetterService } from '../../../services/county-getter.service';
import { UserService } from '../../../services/user.service';

@Component({
    selector: 'navigation',
    templateUrl: './app/components/shared/navigation/navigation.component.html',
    styleUrls: ['./app/components/shared/navigation/navigation.component.css']
})

// this is the navbar at the top of the page
export class NavigationComponent {

    // county getter to pass to the autocomplete component
    countyGetter: CountyGetterService = new CountyGetterService();

    // text for label of autocomplete component
    searchLabelText: string = 'Search for cinema by county: ';

    // determines if the login form is shown or not
    private showingLogin: boolean = false;

    // determines whether the pofile menu is shown or not
    private showingProfileMenu: boolean = false;

    // determines if the signup form is shown or not
    private showingSignup: boolean = false;

    constructor(private router: Router, public loginService: UserService, private location: Location) { }

    hideLogin(): void {
        this.showingLogin = false;
    }

    hideProfileMenu(): void {
        this.showingProfileMenu = false;
    }

    hideSignup(): void {
        this.showingSignup = false;
    }

    // navigates to cinema list after the user searched for
    // a county
    onAutocompleteEmit(text: string): void {
        this.router.navigate(['/cinema-list', text]);
    }

    // hides the login modal
    onLoginEmit(message: string): void {
        if (message === 'cancel') {
            this.hideLogin();
        }
    }

    // hides the signup modal
    onSignupEmit(message: string): void {
        if (message === 'cancel') {
            this.hideSignup();
        }
    }

    showLogin(): void {
        this.showingLogin = true;
    }

    showProfileMenu(): void {
        this.showingProfileMenu = true;
    }

    showSignup(): void {
        this.showingSignup = true;
    }

    // signs the user out and if they are currently viewing their profile
    // navigate to the previous page
    signOut(): void {
        this.hideProfileMenu();
        let path = this.location.path();
        if (path === '/profile') {
            this.location.back();
        }
        this.loginService.signUserOut();
    }

}
