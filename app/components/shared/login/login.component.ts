import { Component, EventEmitter, Input, Output } from '@angular/core';

import { MessageStyles } from '../../../models/message-styles';
import { MessageService } from '../../../services/message.service';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';

@Component({
    selector: 'login',
    templateUrl: './app/components/shared/login/login.component.html',
    styleUrls: [ './app/components/shared/login/login.component.css' ]
})

// a modal holding a login form
export class LoginComponent {

    // emits messages to the parent component
    @Output() emitter: EventEmitter<string> = new EventEmitter<string>();

    // holds the error message if an error occurs
    errorMessage: string = '';

    // the model for the form
    model = new User(1, '', '', '', '');

    // determines if this component
    @Input() show: boolean = false;

    // determines if the error message is shown
    showErrorMessage: boolean = false;

    constructor(private userService: UserService, private messageService: MessageService) {
    }

    // emits a message that this component should be hidden
    cancel(): void {
        this.emitter.emit('cancel');
    }

    hideErrorMessage(): void {
        this.showErrorMessage = false;
    }

    onSubmit(): void {
        // attempt to log the user in
        this.userService.logUserIn(this.model)
            .then((res: boolean) => {
                if (res) {
                    // if login successful, hide error message, clear the form model, hide
                    // this component from view and display a success message
                    this.hideErrorMessage();
                    this.model = new User(1, '', '', '', '');
                    this.cancel();
                    this.messageService.showMessage('Logged in successfully.', MessageStyles.SUCCESS);
                } else {
                    // if login failed, show the appropriate error message
                    this.errorMessage = 'Error. Make sure you entered correct values. If you don\'t have an ' +
                        'account, register first.';
                    this.showErrorMessage = true;
                }
            });
    }
}