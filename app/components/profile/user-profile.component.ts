import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';

import { MessageStyles } from '../../models/message-styles';
import { MessageService } from '../../services/message.service';
import { ReviewService } from '../../services/review.service';
import { Review } from '../../models/review';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'user-profile',
    templateUrl: './app/components/profile/user-profile.component.html',
    styleUrls: [ './app/components/profile/user-profile.component.css' ]
})

// the page where the user can view and edit their profile
export class UserProfileComponent implements OnInit {

    // contains error message generated when attempting
    // to edit profile information
    private editErrorMessage: string = '';

    // determines whether the edit error message
    // is shown
    private editErrorVisible: boolean = false;

    // determines whether the modal containing
    // the form for editing profile information
    // is displayed or not
    private editModalVisible: boolean = false;

    // the model for the edit profile form
    editUserModel = {fullName: '', username: '', newPassword1: '', newPassword2: ''};

    // the error message generated when entering password
    private passwordErrorMessage: string = '';

    // determines whether the password error message is
    // shown or not
    private passwordErrorVisible: boolean = false;

    // determines whether the modal containig the
    // password input is shown or not
    private passwordModalVisible: boolean = false;

    // true if the user has already confirmed their password
    private passwordVerified: boolean = false;

    // the reviews written by the user
    reviews: Review[] = [];

    // the user whose profile is being viewed
    user: User = new User(1, '', '', '', '');

    constructor(private location: Location, private messageService: MessageService,
                private reviewService: ReviewService, private userService: UserService) {
    }

    hideEditError(): void {
        this.editErrorVisible = false;
        this.editErrorMessage = '';
    }

    hideEditModal(): void {
        this.editModalVisible = false;
    }

    hidePasswordError(): void {
        this.passwordErrorVisible = false;
        this.passwordErrorMessage = '';
    }

    hidePasswordModal(): void {
        this.passwordModalVisible = false;
    }

    ngOnInit(): void {
        // get the currently logged in user
        this.user = this.userService.getLoggedInUser();

        // get all reviews written by the currently logged in user
        this.userService.getLoggedInUserReviews(this.reviewService)
            .then((res: Review[]) => {
                // make sure each review is an instance of the Review class,
                // not just an object with the same properties
                for (let r of res) {
                    this.reviews.push(
                        new Review(r.id, r.title, r.body, r.rating, r.movie, r.author)
                    );
                }
            });
    }

    // handles the user clicking on the
    // edit profile button
    onEditClick(): void {
        // if the user has not yet verified their password,
        // ask them to do so
        if (!this.passwordVerified) {
            this.showPasswordModal();
        } else {
            // otherwise, just show the edit form
            this.showEditModal();
        }
    }

    // handles the user submitting the edit profile form
    onEditSubmit(): void {
        // if form was submitted empty, show an error message and return
        if (this.editUserModel.fullName.length == 0 && this.editUserModel.username.length == 0 &&
            this.editUserModel.newPassword1.length == 0) {
            this.showEditError('No data entered.');
            return;
        }
        // if there are no changes between the data entered and the users current profile,
        // show a message and return
        else if (!this.checkChanges()) {
            this.showEditError('The data you entered is not different. Nothing to update.');
            return;
        }
        // if the passwords do not match, show an error message
        else if (this.editUserModel.newPassword1 !== this.editUserModel.newPassword2) {
            this.showEditError('Error - the passwords do not match!');
        } else {
            // if the form passed all above checks
            // create a User instance that will be submitted to the database
            let usr = this.user;
            usr.fullName = this.editUserModel.fullName || usr.fullName;
            usr.username = this.editUserModel.username || usr.username;
            usr.password = this.editUserModel.newPassword1 || usr.password;
            // try to update the user
            this.userService.updateUser(usr, this.reviewService)
                .then((res: boolean) => {
                    if (res) {
                        // if update was successful, show success message and log user out in 3 seconds
                        this.hideEditModal();
                        this.messageService.showMessage('Your account was successfully updated.', MessageStyles.SUCCESS);
                        this.user = this.userService.getLoggedInUser();
                        // change the author on the reviews so if username was
                        // updated, the new username is displayed
                        for (let r of this.reviews) {
                            r.author = this.user;
                        }
                    } else {
                        // if update failed, show an error message
                        let errorMessage = 'An error occurred when updating your profile.';
                        // if the user service has an error message, show it
                        if (this.userService.hasErrorMessage()) {
                            errorMessage = this.userService.getErrorMessage();
                        }
                        this.showEditError(errorMessage);
                    }
                })
        }
    }

    // handles the user submitting their password
    onPasswordSubmit(password: any): void {
        // if password entered is incorrect, show an error message
        if (password !== this.user.password) {
            this.showPasswordError('The password you entered is incorrect');
        } else {
            // if password is correct, hide the password modal
            // and show the edit modal
            this.passwordVerified = true;
            this.hidePasswordError();
            this.hidePasswordModal();
            this.showEditModal();
        }
    }

    showEditError(errorMessage: string): void {
        this.editErrorMessage = errorMessage;
        this.editErrorVisible = true;
    }

    showEditModal(): void {
        this.editModalVisible = true;
    }

    showPasswordError(errorMessage: string): void {
        this.passwordErrorMessage = errorMessage;
        this.passwordErrorVisible = true;
    }

    showPasswordModal(): void {
        this.passwordModalVisible = true;
    }


    // checks if the data entered into the edit form is any different
    // from the users current profile information
    private checkChanges(): boolean {
        return ((this.editUserModel.fullName !== this.user.fullName && this.editUserModel.fullName.length > 0) ||
        (this.editUserModel.username !== this.user.username && this.editUserModel.username.length > 0) ||
        (this.editUserModel.newPassword1 !== this.user.password && this.editUserModel.newPassword1.length > 0));
    }

}
