import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Event, NavigationStart, Router, RoutesRecognized } from '@angular/router';

import { Cinema } from '../../../models/cinema';
import { CinemaService } from '../../../services/cinema.service';
import { Coordinates } from '../../../models/coordinates';

@Component({
    selector: 'cinema-list',
    templateUrl: './app/components/cinema/list/cinema-list.component.html',
    styleUrls: [ './app/components/cinema/list/cinema-list.component.css' ]
})

export class CinemaListComponent implements OnInit {

    // list of cinemas that get displayed
    cinemas: Cinema[] = [];

    // the zoom level for the cinema map
    defaultMapZoom: number = 7;

    // the filter to apply to cinema locations
    filter = '';

    private irelandCoords: Coordinates = { lat: 53.27, lng: -8.24 };

    constructor(private route: ActivatedRoute, private router: Router,
                private cinemaService: CinemaService) {
    }

    ngOnInit(): void {
        this.router.events.subscribe((event: Event) => {
            this.initialize();
        });
    }

    // filter cinemas by county
    private filterCinemas(): void {
        if (this.filter.length === 0) {
            return;
        }
        let filtered: Cinema[] = [];
        for (let cinema of this.cinemas) {
            if (cinema.county.indexOf(this.filter) === 0) {
                filtered.push(cinema);
            }
        }
        this.cinemas = filtered;
    }

    // get all cinemas from database
    private initialize(): void {
        this.cinemaService.getAll()
            .then((res: Cinema[]) => {
                this.cinemas = res;
                this.setFilter();
            })
    }

    private navigateToCinema(id: number) {
        this.router.navigate(['cinema', id]);
    }

    // if the user got to this page by searching by county,
    // only display the cinemas in that county
    private setFilter(): void {
        if (this.route.params['_value']['county']) {
            let county = this.route.params['_value']['county'];
            this.filter = county;
            this.filterCinemas();
        }
    }

}