import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Cinema } from '../../models/cinema';
import { CinemaService } from '../../services/cinema.service';
import { Movie } from '../../models/movie';
import { MovieService } from '../../services/movie.service';

@Component({
    selector: 'cinema',
    templateUrl: './app/components/cinema/cinema.component.html',
    styleUrls: [ './app/components/cinema/cinema.component.css' ]
})

export class CinemaComponent implements OnInit {

    // the cinema this component displays information about
    private cinema: Cinema = new Cinema(1, '', '', '', {lat: 1, lng: 1});

    // list of movies currently playing
    currentMovies: Movie[] = [];

    // list of upcoming movies
    futureMovies: Movie[] = [];

    constructor(private route: ActivatedRoute, private cinemaService: CinemaService, private movieService: MovieService,
        private router: Router) {
    }

    // navigates to a movie's page
    navigateToMovie(id: number): void {
        this.router.navigate(['movie', id]);
    }

    // get the id from the url and get the data of the cinema
    // with that id, then get random movies to display as current/future movies
    ngOnInit(): void {
        if (this.route.params['_value']['id']) {
            let id: number = +this.route.params['_value']['id'];
            this.cinemaService.getOne(id)
                .then((res: Cinema) => {
                    this.cinema = res;
                });
            // pick 4 random movies to serve as current movies
            this.movieService.getManyRandom(4)
                .then((res: Movie[]) => {
                    for (let m of res) {
                        let movie = new Movie(m.id, m.title, m.rating, m.length, m.director, m.cast, m.images, m.trailer);
                        this.currentMovies.push(movie);
                    }
                });
            // pick 4 movies to server as future movies
            this.movieService.getManyRandom(4)
                .then((res: Movie[]) => {
                    for (let m of res) {
                        let movie = new Movie(m.id, m.title, m.rating, m.length, m.director, m.cast, m.images, m.trailer);
                        this.futureMovies.push(movie);
                    }
                });
        }
    }

}
