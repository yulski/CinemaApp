import { Component, OnInit } from '@angular/core';

import { MessageService } from '../../services/message.service';
import { MessageStyles } from '../../models/message-styles';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'my-app',
    templateUrl: './app/components/app/app.component.html',
    styleUrls: ['./app/components/app/app.component.css']
})

// The root component
export class AppComponent implements OnInit {

    // For choosing message styles
    messageStyles = MessageStyles;

    // the timeout set for clearning messages
    timeout;

    constructor(private messageService: MessageService, private userService: UserService) {
    }

    ngOnInit(): void {
        // user is stored in local storage so sign out on init
        this.userService.signUserOut();
        // display any emitted messages for 3 seconds
        this.messageService.messageEmitter.subscribe((str: string) => {
            if (this.messageService.showingMessage) {
                this.timeout = setTimeout(() => {
                    this.messageService.clearMessage();
                }, 3000);
            } else {
                clearTimeout(this.timeout);
            }
        });
    }
}
