import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { MessageService } from '../../services/message.service';
import { MessageStyles } from '../../models/message-styles';
import { Movie } from '../../models/movie';
import { MovieService } from '../../services/movie.service';
import { Review } from '../../models/review';
import { ReviewService } from '../../services/review.service';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'movie-component',
    templateUrl: './app/components/movie/movie.component.html',
    styleUrls: [ './app/components/movie/movie.component.css' ]
})

export class MovieComponent implements OnInit {

    // determines if user is allowed to review this movie
    allowWritingReview: boolean = false;

    // true if the movie's rating has a half-star
    halfStar: boolean = false;

    // the movie this component shows info for
    movie: Movie = new Movie(1, '', 1, 1, '', [], [], '');

    // the text that gets displayed when the user isn't allowed to
    // write a review
    noReviewText: string = 'You must be logged in to review this movie.';

    // the model for review form
    reviewModel = new Review(1, '', '', 1, null, null);

    // this movies reviews
    reviews: Review[] = [];

    // an array representing the star rating this movie got
    stars: number[] = [];

    // determines if the review writing form id displayed
    showWriteReview: boolean = false;

    // the url of the movie's trailer
    trailerUrl: SafeResourceUrl;

    constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer, private messageService: MessageService,
                private movieService: MovieService, private reviewService: ReviewService,
                private userService: UserService) {
    }

    // hides the review writing form
    hideWriteReviewModal(): void {
        this.showWriteReview = false;
    }

    ngOnInit(): void {
        // gets movie id from the url and gets the movie with that id
        // from the movie service
        if (this.route.params['_value']['id']) {
            let id: number = +this.route.params['_value']['id'];
            this.movieService.getOne(id)
                .then((res: Movie) => {
                    // set the data for this movie from the retrieved data item
                    this.movie = res;
                    this.reviewModel.movie = this.movie;
                    this.trailerUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.movie.trailer);
                    this.setRating();
                    // get the reviews for this movie
                    this.getMovieReviews()
                        .then((rev: Review[]) => {
                            for (let r of rev) {
                                this.reviews.push(
                                    new Review(r.id, r.title, r.body, r.rating, r.movie, r.author)
                                );
                            }
                        });
                    if(this.userService.isUserLoggedIn()) {
                        if (this.userService.getLoggedInUser().reviews.length === 0) {
                            this.allowWritingReview = true;
                        } else {
                            this.handleCurrentUserReviews();
                        }
                    }
                });
            // set the author for review model as the currently logged in user
            this.reviewModel.author = this.userService.getLoggedInUser();
            this.userService.userEmitter.subscribe((user: User) => {
                this.reviewModel.author = user;
                if(user != null) {
                    this.handleCurrentUserReviews();
                } else {
                    this.noReviewText = 'You must be logged in to review this movie.';
                    this.allowWritingReview = false;
                }
            });
        }
    }

    onSubmit(): void {
        this.hideWriteReviewModal();
        this.messageService.showMessage('Submitting review...', MessageStyles.NONE);
        // post the review and hide the modal
        this.reviewService.post(this.reviewModel)
            .then((res: boolean) => {
                if (res) {
                    this.messageService.clearMessage();
                    // if submission successful, show message
                    this.messageService.showMessage('Review submitted successfully.', MessageStyles.SUCCESS);

                    // add the review to the reviews on this page
                    this.reviews.push(this.reviewModel);

                    // reset model
                    this.reviewModel = new Review(1, '', '', 1, this.movie, this.userService.getLoggedInUser());

                    // update this movie's rating to account for the new review
                    this.movieService.getOne(this.movie.id)
                        .then((res: Movie) => {
                            this.movie.rating = res.rating;
                            this.setRating();
                        });

                    // if user submitted review successfully, don't allow them to write another one
                    this.noReviewText = 'You already reviewed this movie.';
                    this.allowWritingReview = false;
                } else {
                    // if submission failed, show message
                    this.messageService.showMessage('Failed to submit review.', MessageStyles.DANGER);
                }
            })
    }

    // show the review writing form if the user is logged in
    // otherwise, display an error message
    showWriteReviewModal(): void {
        if (this.userService.isUserLoggedIn()) {
            this.showWriteReview = true;
        }
    }

    // gets the reviews for this movie
    private getMovieReviews(): Promise<Review[]> {
        return this.reviewService.getAll()
            .then((res: Review[]) => {
                return res.filter((review: Review) => {
                    return review.movie.id === this.movie.id;
                })
            });
    }

    private handleCurrentUserReviews(): void {
        // allow user to write a review if they haven't reviewed any movies or this movie
        // prevent them from writing a review if they have reviewed this movie
        if (this.userService.getLoggedInUser().reviews.length === 0) {
            this.allowWritingReview = true;
            return;
        }
        this.hasUserReviewedMovie().then((res: boolean[]) => {
            for (let val of res) {
                this.allowWritingReview = !val;
                if (val) {
                    this.noReviewText = 'You already reviewed this movie.';
                    break;
                }
            }
        });
    }

    // checks if a user has reviewed this movie
    private hasUserReviewedMovie(): Promise<boolean[]> { // : Promise<Promise<boolean>[]> {
        let user = this.userService.getLoggedInUser();
        let promises: Promise<boolean>[] = [];
        if (!user) {
            return Promise.all(promises);
        }
        let reviews = user.reviews;
        // for every review the logged in user has written, get that review
        // and compare its movie's id to this movie's id
        for (let id of reviews) {
            promises.push(this.reviewService.getOne(id)
                .then((res: Review) => {
                    return res.movie.id === this.movie.id;
                }));
        }
        return Promise.all(promises);
    }

    // set the array and halfStar properties representing the
    // movie's rating
    private setRating(): void {
        this.stars = new Array(Math.floor(this.movie.rating));
        if (this.movie.rating % 1 >= 0.5) {
            this.halfStar = true;
        }
    }
}
