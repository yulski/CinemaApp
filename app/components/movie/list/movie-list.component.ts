import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Movie } from '../../../models/movie';
import { MovieService } from '../../../services/movie.service';

@Component({
    selector: 'movie-list',
    templateUrl: './app/components/movie/list/movie-list.component.html',
    styleUrls: [ './app/components/movie/list/movie-list.component.css' ]
})

// page which displays list of all movies
export class MovieListComponent implements OnInit{

    // the movies which are displayed
    displayedMovies: Movie[] = [];

    // the search term to filter movies by
    filter: string = '';

    // list of all movies that get displayed
    movies: Movie[] = [];

    // list of movie titles for autocomplete component
    titles: string[] = [];

    constructor(private movieService: MovieService, private router: Router) { }

    // naviagates to a movie's page
    navigateToMovie(id: number): void {
        this.router.navigate(['movie', id]);
    }

    // on init get all movies from the database
    ngOnInit(): void {
        this.movieService.getAll()
            .then((res: Movie[]) => {
                this.movies = res;
                this.displayedMovies = res;
                this.titlesInit();
            });
    }

    // handles the user searching for a movie
    onMovieSearch(selection: string): void {
        this.filter = selection;
        // if user enters a search term, search
        // for a matching movie title
        this.applyFilter();
    }

    // resets the displayed movies to show all movies
    resetDisplayedMovies(): void {
        this.displayedMovies = this.movies;
    }

    // filters the movies that get displayed
    // to the user by the search term the user
    // typed in
    private applyFilter(): void {
        let matches: Movie[] = [];
        for (let movie of this.movies) {
            if (movie.title.toLowerCase().indexOf(this.filter.toLowerCase()) === 0) {
                matches.push(movie);
            }
        }
        this.displayedMovies = matches;
    }

    // gets all movie titles and saves them in the
    // array titles
    private titlesInit() {
        let arr: string[] = [];
        for (let movie of this.movies) {
            arr.push(movie.title);
        }
        this.titles = arr;
    }
}
