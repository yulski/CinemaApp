import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CinemaComponent } from './components/cinema/cinema.component';
import { CinemaListComponent } from './components/cinema/list/cinema-list.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MovieComponent } from './components/movie/movie.component';
import { MovieListComponent } from './components/movie/list/movie-list.component';
import { UserProfileComponent } from './components/profile/user-profile.component';

const ROUTES: Routes = [
    {
        path: 'cinema/:id',
        component: CinemaComponent
    },
    {
        path: 'cinema-list',
        component: CinemaListComponent
    },
    {
        path: 'cinema-list/:county',
        component: CinemaListComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'movie',
        component: MovieComponent
    },
    {
        path: 'movie/:id',
        component: MovieComponent
    },
    {
        path: 'movie-list',
        component: MovieListComponent
    },
    {
        path: 'profile',
        component: UserProfileComponent
    },
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(ROUTES);
