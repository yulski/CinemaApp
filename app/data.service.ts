import { InMemoryDbService } from 'angular-in-memory-web-api';
import { User } from './models/user';
import { Cinema } from './models/cinema';
import { Movie } from './models/movie';
import { Review } from './models/review';

export class DataService implements InMemoryDbService {
    createDb() {
        let cinemas: Cinema[] = [
            new Cinema(1, 'Odeon Dublin', 'Dublin', 'Dublin', {lat: 53.349731, lng: -6.260103}),
            new Cinema(2, 'Omniplex Dublin', 'Dublin', 'Dublin', {lat: 53.349759, lng: -6.351428}),
            new Cinema(3, 'Vue Cinemas Dublin', 'Dublin', 'Dublin', {lat: 53.302346, lng: -6.180095}),
            new Cinema(4, 'Galway Cinemas', 'Galway', 'Galway', {lat: 53.270723, lng: -9.057203}),
            new Cinema(5, 'Odeon Cork', 'Cork', 'Cork', {lat: 51.896891, lng: -8.486328}),
            new Cinema(6, 'Portlaoise Cinemas', 'Portlaoise', 'Laois', {lat: 53.033006, lng: -7.298463}),
            new Cinema(7, 'Vue Cinemas Ennis', 'Ennis', 'Clare', {lat: 52.847101, lng: -8.988469}),
            new Cinema(8, 'Ballina Cinemas', 'Ballina', 'Mayo', {lat: 54.114989, lng: -9.154838}),
            new Cinema(9, 'Omniplex Gorey', 'Gorey', 'Wexford', {lat: 52.675747, lng: -6.294364}),
            new Cinema(10, 'Omniplex Mullingar', 'Mullingar', 'Westmeath', {lat: 53.525957, lng: -7.338452}),
            new Cinema(11, 'Odeon Sligo', 'Sligo', 'Sligo', {lat: 54.276463, lng: -8.475789}),
            new Cinema(12, 'Omniplex Cork', 'Cork', 'Cork', {lat: 51.899155, lng: -8.483203}),
            new Cinema(13, 'Limerick City Cinemas', 'Limerick', 'Limerick', {lat: 52.668317, lng: -8.630862}),
            new Cinema(14, 'Navan Cinemas', 'Navan', 'Meath', {lat: 53.647155, lng: -6.696506}),
            new Cinema(15, 'Newcastle Cinemas', 'Newcastle', 'Down', {lat: 54.215971, lng: -5.892026}),
            new Cinema(16, 'Vue Cinemas Bray', 'Bray', 'Wicklow', {lat: 53.201089, lng: -6.110996}),
            new Cinema(17, 'Odeon Tralee', 'Tralee', 'Kerry', {lat: 52.271976, lng: -9.698888}),
            new Cinema(18, 'Wicklow Cinemas', 'Wicklow', 'Wicklow', {lat: 52.980757, lng: -6.045778}),
            new Cinema(19, 'Thurles Cinemas', 'Thurles', 'Tipperary', {lat: 52.678586, lng: -7.815358}),
            new Cinema(20, 'Omniplex Tullamore', 'Tullamore', 'Offaly', {lat: 53.275271, lng: -7.492451})
        ];

        let movies = [
            new Movie(1, 'The Conjuring 2', 3.5, 134, 'James Wan', ['Patrick Wilson', 'Vera Farmiga', 'Madison Wolfe', 'Frances O\'Connor', 'Lauren Esposito'], ['./images/movies/conjuring2_poster_1.jpg', './images/movies/conjuring2_poster_2.jpg', '/images/movies/conjuring2_poster_3.jpg'], 'https://www.youtube.com/embed/VFsmuRPClr4'),
            new Movie(2, 'Jason Bourne', 0, 123, 'Paul Greengrass', ['Matt Damon', 'Alicia Vikander', 'Tommy Lee Jones', 'Julia Stiles', 'Vincent Cassel'], ['./images/movies/jason_bourne_poster_1.jpg', './images/movies/jason_bourne_poster_2.jpg', './images/movies/jason_bourne_poster_3.jpg'], 'https://www.youtube.com/embed/F4gJsKZvqE4'),
            new Movie(3, 'Ben-Hur', 0, 150, 'Timur Bekmambetov', ['Jack Huston', 'Morgan Freeman', 'Toby Kebbell', 'Rodrigo Santoro', 'Nazanin Boniadi'], ['./images/movies/ben-hur_poster_1.jpg', './images/movies/ben-hur_poster_2.jpg', './images/movies/ben-hur_poster_3.jpg'], 'https://www.youtube.com/embed/3BmeR9GYdDU'),
            new Movie(4, 'Warcraft', 2.6, 123, 'Duncan Jones', ['Travis Fimmel', 'Paula Patton', 'Ben Foster', 'Toby Kebbell', 'Dominic Cooper'], ['./images/movies/warcraft_poster_1.jpg', './images/movies/warcraft_poster_2.jpg', './images/movies/warcraft_poster_3.jpg'], 'https://www.youtube.com/embed/RhFMIRuHAL4'),
            new Movie(5, 'Don\'t Breathe', 0, 88, 'Fede Alvarez', ['Stephen Lang', 'Jane Levy', 'Dylan Minnette', 'Daniel Zovatto', 'Sergej Onopko'], ['./images/movies/dont-breathe_poster_1.jpg', './images/movies/dont-breathe_poster_2.png', './images/movies/dont-breathe_poster_3.jpg'], 'https://www.youtube.com/embed/76yBTNDB6vU'),
            new Movie(6, '10 Cloverfield Lane', 4, 106, 'Dan Trachtenberg', ['Mary Elizabeth Winstead', 'John Goodman', 'John Gallagher Jr.', 'Douglas M. Griffin', 'Cindy Hogan'], ['./images/movies/10clln_poster_1.jpg', './images/movies/10clln_poster_2.jpg', './images/movies/10clln_poster_3.jpg'], 'https://www.youtube.com/embed/saHzng8fxLs'),
            new Movie(7, 'Suicide Squad', 2, 130, 'David Ayer', ['Margot Robbie', 'Jared Leto', 'Will Smith', 'Cara Delevingne', 'Jai Courtney'], ['./images/movies/ssquad_poster_1.jpg', './images/movies/ssquad_poster_2.jpg', './images/movies/ssquad_poster_3.jpg'], 'https://www.youtube.com/embed/CmRih_VtVAs'),
            new Movie(8, 'Finding Dory', 4, 105, 'Andrew Stanton & Angus MacLane', ['Ellen DeGeneres', 'Ty Burrell', 'Ed O\'Neil', 'Kaitlin Olson', 'Albert Brooks'], ['./images/movies/finding-dory_poster_1.jpg', './images/movies/finding-dory_poster_2.jpg', './images/movies/finding-dory_poster_3.jpg', './images/movies/finding-dory_poster_4.jpg', './images/movies/finding-dory_poster_5.jpg'], 'https://www.youtube.com/embed/3JNLwlcPBPI'),
            new Movie(9, 'Midnight Special', 5, 112, 'Jeff Nichols', ['Michael Shannon', 'Kirsten Dunst', 'Joel Edgerton', 'Jaeden Lieberher', 'Adam Driver'], ['./images/movies/midnight-special_poster_1.jpg', './images/movies/midnight-special_poster_2.jpg', './images/movies/midnight-special_poster_3.jpg'], 'https://www.youtube.com/embed/oVgxxdu-gJc'),
            new Movie(10, 'Captain Fantastic', 0, 118, 'Matt Ross', ['Viggo Mortensen', 'George MacKay', 'Annalise Basso', 'Samantha Isler', 'Shree Crooks'], ['./images/movies/cpt-fantastic_poster_1.jpg', './images/movies/cpt-fantastic_poster_2.jpg', './images/movies/cpt-fantastic_poster_3.jpg'], 'https://www.youtube.com/embed/D1kH4OMIOMc'),
            new Movie(11, 'The Purge: Election Year', 0, 109, 'James DeMonaco', ['Frank Grillo', 'Elizabeth Mitchell', 'Mykelti Williamson', 'Edwin Hoodge', 'Betty Gabriel'], ['./images/movies/the_purge_ey_poster_1.jpg', './images/movies/the_purge_ey_poster_2.jpg', './images/movies/the_purge_ey_poster_3.jpg', './images/movies/the_purge_ey_poster_4.jpg'], 'https://www.youtube.com/embed/RXMp9fBomJw'),
            new Movie(12, 'True Crimes', 0, 92, 'Alexandros Avranas', ['Jim Carrey', 'Charlotte Gainsbourg', 'Agata Kulesza', 'Marton Csokas', 'Zbigniew Zamachowski'], ['./images/movies/true_crimes_poster_1.jpg', './images/movies/true_crimes_poster_2.jpg', './images/movies/true_crimes_poster_3.jpg'], 'https://www.youtube.com/embed/TNOdzFTzefo')
        ];

        let users: User[] = [
            new User(1, 'John Smith', 'john_smith_2016', 'jsmith123@gmail.com', 'pass-word', [1, 2, 9], './images/profiles/profile_picture_1.png'),
            new User(2, 'Jane Smith', 'jane smith', 'jane1010@yahoo.com', '123pass123', [3, 5, 8, 10], './images/profiles/profile_picture_2.png'),
            new User(3, 'Karl Karlsson', 'user900', 'an_email@outlook.com', 'my_password', [4, 6, 7], './images/profiles/profile_picture_3.png')
        ];

        let reviews = [
            new Review(1, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 3, movies[0], users[0]),
            new Review(2, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 2, movies[3], users[0]),
            new Review(3, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 4, movies[7], users[1]),
            new Review(4, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 3, movies[3], users[2]),
            new Review(5, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 4, movies[0], users[1]),
            new Review(6, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 5, movies[8], users[2]),
            new Review(7, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 3, movies[0], users[2]),
            new Review(8, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 3, movies[3], users[1]),
            new Review(9, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 4, movies[5], users[0]),
            new Review(10, 'Lorem Ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 2, movies[6], users[1])
        ];

        return {cinemas, movies, reviews, users};
    }
}