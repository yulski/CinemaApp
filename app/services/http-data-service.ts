import { Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { DataContainer } from '../models/data-container';
import { HttpService } from './http-service';

// specifies how HTTP services should behave
export abstract class HttpDataService<T extends DataContainer> extends HttpService {

    // returns all data from the database
    getAll(): Promise<T[]> {
        return this.http.get(this.url)
            .toPromise()
            .then((res: Response) => {
                return res.json().data as T[];
            })
            .catch(this.handleError);
    }

    // returns a specific number of items from the database
    // if grabFromEnd is true, returns the last [count] items rather
    // than the first
    // returns all items if the number of data items in database
    // is < [count]
    getMany(count: number, grabFromEnd?: boolean): Promise<T[]> {
        return this.http.get(this.url)
            .toPromise()
            .then((res: Response) => {
                let data: T[] = res.json().data as T[];
                if (grabFromEnd) {
                    data.reverse();
                }
                if (count < data.length) {
                    return data.slice(0, count);
                } else {
                    return data;
                }
            })
    }

    // returns an array of randomly picked items from the database
    // the array is [count] items long
    // if count is more than the number of items, the entire
    // set of items is returned
    getManyRandom(count: number): Promise<T[]> {
        return this.http.get(this.url)
            .toPromise()
            .then((res: Response) => {
                let data = res.json().data as T[];
                if (count >= data.length) {
                    return data;
                }

                let arr: T[] = [];
                let index: number;
                for (let i=0; i<count; i++) {
                    // generate a random index
                    index = Math.floor(Math.random() * data.length);
                    // remove the item at the index from the data array and
                    // add it to the return array
                    arr.push(data.splice(index, 1)[0]);
                }
                return arr;

            })
            .catch(this.handleError);
    }

    // returns one data item with the specified id
    getOne(id: number): Promise<T> {
        return this.http.get(this.url + '/' + id)
            .toPromise()
            .then((res: Response) => {
                return res.json().data as T;
            })
            .catch(this.handleError);
    }

    // posts an item to the database
    // the id is first reserved and then the item is saved
    // under that id
    post(t: T): Promise<boolean> {
        return this.reserveId()
            .then((res: number) => {
                t.id = res;
                return this.http.post(this.url, t)
                    .toPromise()
                    .then((res: Response) => {
                        return res.ok;
                    })
                    .catch(this.handleError);
            })
            .catch(this.handleError);
    }

    // inserts an empty object into the database
    // and returns the id that the object was saved with
    // this provides a valid id that a new item can be
    // saved with
    reserveId(): Promise<number> {
        return this.http.post(this.url, {})
            .toPromise()
            .then((res: Response) => {
                return res.json().data['id'] as number;
            })
            .catch(this.handleError);
    }

    // updates an item in the database with the newly provided value
    update(t: T): Promise<boolean> {
        return this.http.put(`${this.url}/${t.id}`, t)
            .toPromise()
            .then((res: Response) => {
                return res.ok;
            })
            .catch(this.handleError);
    }

}
