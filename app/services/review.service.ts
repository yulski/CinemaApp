import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { HttpDataService } from './http-data-service';
import { MovieService } from './movie.service';
import { Review } from '../models/review';

@Injectable()
export class ReviewService extends HttpDataService<Review> {

    private movieService: MovieService;

    protected url: string = '/api/reviews';

    constructor(http: Http) {
        super(http);
        // create a movie service that will be used to update movie ratings
        this.movieService = new MovieService(this.http);
    }

    // override of post that recalculate the movie's rating to
    // account for the newly posted review
    public post(review: Review): Promise<boolean> {
        return this.reserveId()
            .then((res: number) => {
                review.id = res;
                return this.http.post(this.url, review)
                    .toPromise()
                    .then((res: Response) => {
                        return this.movieService.recalculateRating(review.movie.id, this)
                        // wait for movie score to be recalculated before returning
                            .then(val => res.ok);
                    })
                    .catch(this.handleError);
            })
            .catch(this.handleError);
    }

    // override of update that recalculates the movie's rating to
    // account for the updated review
    public update(review: Review): Promise<boolean> {
        return this.http.put(`${this.url}/${review.id}`, review)
            .toPromise()
            .then((res: Response) => {
                return this.movieService.recalculateRating(review.movie.id, this)
                    .then(val => res.ok);
            })
            .catch(this.handleError)
    }

}
