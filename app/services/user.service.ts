import { EventEmitter, Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { CoolLocalStorage } from 'angular2-cool-storage';
import 'rxjs/add/operator/toPromise';

import { HttpService } from './http-service';
import { Review } from '../models/review';
import { ReviewService } from './review.service';
import { User } from '../models/user';

// handles user operations, such as login and signup
@Injectable()
export class UserService extends HttpService {

    // the database url
    protected url: string = '/api/users';

    // emits User on login/logout events
    userEmitter: EventEmitter<User> = new EventEmitter<User>();

    constructor(http: Http, private localStorage: CoolLocalStorage) {
        super(http);
    }

    // returns all users
    getAll(): Promise<User[]> {
        this.clearErrorMessage();
        return this.http.get(this.url)
            .toPromise()
            .then((res: Response) => {
                return res.json().data as User[];
            })
            .catch(this.handleError);
    }

    // returns the currently logged in user
    getLoggedInUser(): User {
        this.clearErrorMessage();
        return JSON.parse(this.localStorage.getItem('current-user')) as User;
    }

    // returns an array of reviews written by the user
    // currently logged in
    getLoggedInUserReviews(reviewService: ReviewService): Promise<Review[]> {
        this.clearErrorMessage();
        return reviewService.getAll()
            .then((res: Review[]) => {
                return res.filter((rev: Review) => {
                    return rev.author.id === this.getLoggedInUser().id;
                });
            })
            .catch(this.handleError);
    }

    // returns true if a user is currently logged in
    isUserLoggedIn(): boolean {
        this.clearErrorMessage();
        return this.localStorage.getItem('user-logged-in') === 'yes';
    }

    // tries to log user in
    logUserIn(login: User): Promise<boolean> {
        this.clearErrorMessage();
        return this.http.get(this.url)
            .toPromise()
            .then((res: Response) => {
                // find if the user who is trying to log in exists in the database
                return this.findUser(login.username, login.email, login.password)
                    .then((usr: User) => {
                        if (usr != null) {
                            // save user in local storage if user was found
                            this.localStorage.setItem('user-logged-in', 'yes');
                            this.localStorage.setObject('current-user', usr);
                        } else {
                            this.lastErrorMessage = 'Login failed. No such user';
                        }
                        console.log('emitting');
                        this.userEmitter.emit(usr);
                        // return true if a user was found
                        return usr != null;
                    })
                    .catch(this.handleError);
            })
            .catch(this.handleError);
    }

    // signs the user out by removing them from local storage
    signUserOut(): void {
        this.clearErrorMessage();
        this.localStorage.setItem('user-logged-in', 'no');
        this.localStorage.setObject('current-user', null);
        this.userEmitter.emit(null);
    }

    // signs the user up
    signUserUp(user: User): Promise<boolean> {
        // give the new user a random profile picture
        user.profilePicture = this.getRandomProfilePicture();
        this.clearErrorMessage();
        // first, try to find if the user is already signed up
        return this.findUser(user.username, user.email)
            .then((respUsr: User) => {
                if (respUsr == null) {
                    // if user is not signed up, reserve an id
                    // for the user to be posted
                    return this.reserveId()
                        .then((res: number) => {
                            // set the user's id to the id that was reserved
                            user.id = res;
                            // post the user to the database
                            return this.http.post(this.url, user)
                                .toPromise()
                                .then((res: Response) => {
                                    return res.ok;
                                })
                                .catch(this.handleError);
                        })
                        .catch(this.handleError);
                } else {
                    // return false if the user is already signed up
                    this.lastErrorMessage = 'User is already signed up.';
                    return Promise.resolve(false);
                }
            })
            .catch(this.handleError);
    }

    // updates a user
    updateUser(user: User, reviewService: ReviewService): Promise<boolean> {
        this.clearErrorMessage();
        // checks if the username is a valid one
        return this.isUsernameValid(user.id, user.username)
            .then((res: boolean) => {
                if (res) {
                    // if the username is valid, update the user
                    return this.http.put(`${this.url}/${user.id}`, user)
                        .toPromise()
                        .then((res: Response) => {
                            // if the update was successful and the updated user is currently logged
                            // in (which they are) updated the logged in user
                            if (this.getLoggedInUser().id == user.id && res.ok) {
                                this.localStorage.setObject('current-user', user);
                            }
                            // update all reviews written by the user
                            this.updateUserReviews(user, reviewService);
                            return res.ok;
                        })
                }
                // if username is invalid, set an error message and return false
                this.lastErrorMessage = 'Username \'' + user.username + '\' is taken.';
                return false;
            })
            .catch(this.handleError);
    }

    // searches the database in an attemt to find a user with a matching username, email and maybe password
    private findUser(username: string, email: string, password?: string): Promise<User> {
        this.clearErrorMessage();
        // first, get all users
        return this.http.get(this.url)
            .toPromise()
            .then((res: Response) => {
                let users = res.json().data as User[];
                // filter the entire users array and find the users who have a matching username
                // or email
                let matches: User[] = users.filter((user: User, i: number, arr: User[]) => {
                    let ret: boolean = (user.username === username || user.email === email);
                    // if password was provided, also check against the password
                    if (password) {
                        ret = ret && user.password === password;
                    }
                    return ret;
                });
                // if no users match the description, enter null into
                // matches array
                if (matches.length === 0) {
                    matches.push(null);
                }
                // return the first match (null in case no users were found)
                return matches[0];
            })
            .catch(this.handleError);
    }

    // returns a random profile picture location
    private getRandomProfilePicture(): string {
        let pictures: string[] = ['./images/profiles/profile_picture_1.png', './images/profiles/profile_picture_2.png',
            './images/profiles/profile_picture_3.png', './images/profiles/profile_picture_4.png'];
        let randIndex = Math.floor(Math.random() * 4);
        return pictures[randIndex];
    }

    // checks if a passed username is valid to use
    private isUsernameValid(id: number, username: string): Promise<boolean> {
        this.clearErrorMessage();
        // get all users
        return this.getAll()
            .then((res: User[]) => {
                // check that no user (other than the user with the passed id)
                // has the passed username
                return (res.some((elem) => {
                    return elem.username === username && elem.id !== id;
                }) == false);
            })
            .catch(this.handleError);
    }

    // saves an empty object in the database
    // and returns the id that object was saved with
    private reserveId(): Promise<number> {
        this.clearErrorMessage();
        return this.http.post(this.url, {})
            .toPromise()
            .then((res: Response) => {
                return res.json().data['id'] as number;
            })
            .catch(this.handleError);
    }

    // updates the author of all reviews written by the user
    // used after user changes username to update author username on their reviews
    private updateUserReviews(user: User, reviewService: ReviewService): void {
        for (let id of user.reviews) {
            reviewService.getOne(id)
                .then((rev: Review) => {
                    rev.author = user;
                    reviewService.update(rev);
                });
        }
    }

}
