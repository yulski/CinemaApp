import { Injectable } from '@angular/core';

import { ItemGetter } from './item-getter';

// returns list of irish counties
@Injectable()
export class CountyGetterService implements ItemGetter<string> {
    // list of Irish counties
    items: string[] = [
        'Antrim', 'Armagh', 'Carlow', 'Cavan', 'Clare', 'Cork', 'Derry', 'Donegal', 'Down',
        'Dublin', 'Fermanagh', 'Galway', 'Kerry', 'Kildare', 'Kilkenny', 'Laois', 'Leitrim',
        'Limerick', 'Longford', 'Louth', 'Mayo', 'Meath', 'Monaghan', 'Offaly', 'Roscommon',
        'Sligo', 'Tipperary', 'Tyrone', 'Waterford', 'Westmeath', 'Wexford', 'Wicklow'
    ];

    // returns the county list wrapped in a promise
    getItems(): Promise<string[]> {
        return Promise.resolve(this.items);
    }

    // error handling
    handleError(err: string): void {
        console.log('ERROR!: ' + err);
    }
}
