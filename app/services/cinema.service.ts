import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Cinema } from '../models/cinema';
import { HttpDataService } from './http-data-service';

@Injectable()
export class CinemaService extends HttpDataService<Cinema> {

    protected url: string = '/api/cinemas';

    constructor(http: Http) {
        super(http);
    }

}
