import { EventEmitter, Injectable } from '@angular/core';

import { MessageStyles } from '../models/message-styles';

// displays messsages to the user
@Injectable()
export class MessageService {
    // the current message that should be displayed
    message: string = '';

    // emits a message that a new message should be displayed
    messageEmitter: EventEmitter<string> = new EventEmitter<string>();

    // the style of the message that should be displayed
    messageStyle: MessageStyles = MessageStyles.NONE;

    // determines if the message is shown or not
    showingMessage: boolean = false;

    // clears the message and hides this component
    clearMessage(): void {
        this.message = '';
        this.showingMessage = false;
        this.messageStyle = MessageStyles.NONE;
        this.messageEmitter.emit('cleared message');
    }

    // displays a message and emits a message about this
    showMessage(message: string, style: MessageStyles): void {
        this.message = message;
        this.messageStyle = style;
        this.showingMessage = true;
        this.messageEmitter.emit('new message');
    }
}
