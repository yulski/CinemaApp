import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

// A service which deals with HTTP requests
export abstract class HttpService {

    // URL for HTTP requests
    protected url: string;

    // holds a message outlining the last error that occurred
    protected lastErrorMessage: string = '';

    // Must have a Http object for executing HTTP
    // requests & responses
    constructor(protected http: Http) {
    }

    // clears the last error message
    clearErrorMessage(): void {
        this.lastErrorMessage = '';
    }

    // returns the last error message
    getErrorMessage(): string {
        return this.lastErrorMessage;
    }

    // returns true of there is an error message
    hasErrorMessage(): boolean {
        return this.lastErrorMessage.length > 0;
    }

    // error handling for failed requests &
    // HTTP errors
    protected handleError(err: any) {
        console.log(err);
        this.lastErrorMessage = err as string;
    }

}
