// an ItemGetter which gets a list of items
// and returns them
export interface ItemGetter<T> {
    // may have a list of items
    items?: T[];

    // may have a url from which to get the items
    url?: string;

    // returns items
    getItems(): Promise<T[]>;

    // handles errors
    handleError(err: string): void;
}
