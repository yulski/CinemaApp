import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import "rxjs/add/operator/toPromise";

import { HttpDataService } from './http-data-service';
import { Movie } from '../models/movie';
import { Review } from '../models/review';
import { ReviewService } from './review.service';

@Injectable()
export class MovieService extends HttpDataService<Movie> {

    protected url: string = '/api/movies';

    constructor(http: Http) {
        super(http);
    }

    // recalculates the movie's rating to account for all reviews of it
    recalculateRating(id: number, reviewService: ReviewService): Promise<boolean> {
        // get all reviews
        return reviewService.getAll()
            .then((res: Review[]) => {
                // apply filter to get reviews for this movie only
                let thisMovieReviews: Review[] = res.filter((review: Review) => {
                    return review.movie.id === id;
                });
                // count of this movie's reviews
                let reviewCount: number = +thisMovieReviews.length;
                // calculate total rating from all reviews
                let totalRating: number = +thisMovieReviews.reduce((prev: Review, curr: Review) => {
                    prev.rating = (+prev.rating + +curr.rating);
                    return prev;
                }).rating;
                // get this movie's set its rating to the new value
                // and update the database with this new value
                return this.getOne(id)
                    .then((mov: Movie) => {
                        mov.rating = (totalRating / reviewCount);
                        return this.update(mov)
                            .then((success: boolean) => {
                                return success;
                            });
                });
            });
    }

}
